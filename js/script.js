const lightTheme = document.querySelector('link[media*=light]');
const darkTheme = document.querySelector('link[media*=dark]');
const switcher = document.querySelector('.switcher');
const darkSchemeMedia = matchMedia('(prefers-color-scheme: dark)');

(function setSwitcher() {
    const savedScheme = getSavedScheme();

    if (!!savedScheme) {
        const currentRadio = document.querySelector(`.switcher__radio[value=${savedScheme}]`);
        currentRadio.checked = true;
    }
    switcher.addEventListener('change', (e) => setScheme(e.target.value));
})();

(function setupScheme() {
    const savedScheme = getSavedScheme();
    const systemScheme = getSystemScheme();

    if (!savedScheme) return;
    savedScheme !== systemScheme && setScheme(savedScheme);
})();

function setScheme(scheme) {
    switchMeda(scheme);
    scheme === 'auto' ? clearScheme() : saveScheme(scheme);
}

function switchMeda(scheme) {
    let light;
    let dark;
    if (scheme === 'auto') {
        light = '(prefers-color-scheme: light)';
        dark = '(prefers-color-scheme: dark)';
    } else {
        light = (scheme === 'light') ? 'all' : 'not all';
        dark = (scheme === 'dark') ? 'all' : 'not all';
    }

    lightTheme.media = light;
    darkTheme.media = dark;
}

function getSystemScheme() {
    const browserScheme = darkSchemeMedia.matches;
    return browserScheme ? 'dark' : 'light';
}

function getSavedScheme() {
    return localStorage.getItem('color-scheme');
};

function saveScheme(scheme) {
    localStorage.setItem('color-scheme', scheme);
};

function clearScheme() {
    localStorage.removeItem('color-scheme');
}